Search Wizard
=================
The Search wizard was developed by International Potato Center (CIP) and using Apache 2.0 licence.

Source code from: https://gitlab.com/CIP-Development/grin-global_client_international

Quick training video: https://www.youtube.com/watch?v=qhtG77qc-Z0

For support (training, improvements or fixs) contact to: e.rojas@cgiar.org

